package exampleSqlPractice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Example_Sql {

	public static void main(String[] args) {
		Connection conn = null; 
		Statement stmt = null;
		
		String url = "jdbc:mysql://localhost:3306/dbmicrotech";
		String user = "root";
		String password = "root";

		
		try {
			// for establish connection
			conn = DriverManager.getConnection(url, user, password);
			if (conn != null) {
			
			System.out.println("We are connected");
			System.out.println(conn);
			
			stmt = conn.createStatement();// for sending stmt to database
			String sql = "SELECT prodId, prodName, prodPrice FROM tblproduct";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				int pId = rs.getInt("ProdId");
				System.out.println(pId);
				
			}
			}
		} catch (SQLException e) {  
			System.out.println(e.getMessage());
			System.out.println("Wrong Crdedentials");
			System.out.println("Testing");
			
		}


	}//main

}//class
